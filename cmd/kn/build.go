package main

import (
	"gitlab.com/rwx.gg/cmdtab"
	"gitlab.com/rwx.gg/kn/knowledge"
	"gitlab.com/rwx.gg/kn/knowledge/work/pandoc"
)

func init() {
	_cmd := cmdtab.New("build")
	_cmd.Version = `0.0.1`
	_cmd.Usage = `<basedir>`
	_cmd.Summary = `build a knowledge base`
	_cmd.Description = ``
	_cmd.Method = func(args []string) error {
		if len(args) == 1 {
			Base = args[0]
		}
		b, err := knowledge.NewBase(Base)
		if err != nil {
			return err
		}
		return pandoc.Build(b)
	}
}
