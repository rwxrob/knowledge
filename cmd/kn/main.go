package main

import (
	"os"

	"gitlab.com/rwx.gg/cmdtab"
)

var Base string

func init() {
	Base = os.Getenv("README")
	if Base == "" {
		Base = os.Getenv("KNOWLEDGE")
	}
}

func main() { cmdtab.Execute("kn") }
