package main

import "gitlab.com/rwx.gg/cmdtab"

func init() {
	_cmd := cmdtab.New("kn", "render", "build")
	_cmd.Version = `0.0.1`
	_cmd.Usage = `[<subcommand>]`
	_cmd.Summary = `create, manage, search, share, and discover knowledge source`
	_cmd.Description = `
			The *README.World* Knowledge Management tool is a command line
			utility designed to help you create, manage, search, share, and
			discover other decentralized knowledge bases like yours
      on the Internet, local area networks, and independent registries such as the
      README.World Exchange.
  `
}
