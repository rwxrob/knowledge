package main

import (
	"strings"

	"gitlab.com/rwx.gg/cmdtab"
	"gitlab.com/rwx.gg/kn/knowledge"
	"gitlab.com/rwx.gg/kn/knowledge/work/pandoc"
)

func init() {
	_cmd := cmdtab.New("render")
	_cmd.Version = `0.0.1`
	_cmd.Usage = `<node> [<basedir>]`
	_cmd.Summary = `render a specific node by name/ID`
	_cmd.Description = ``
	_cmd.Method = func(args []string) error {
		if len(args) == 0 {
			return _cmd.UsageError()
		}
		ref := args[0]
		if strings.HasSuffix(ref, "/README.md") {
			ref = ref[0 : len(ref)-10]
		}
		if len(args) == 2 {
			Base = args[1]
		}
		b, err := knowledge.NewBase(Base)
		n, err := b.Node(ref)
		if err != nil {
			return err
		}
		return pandoc.Render(n)
	}
}
