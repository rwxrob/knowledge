package knowledge

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/rwx.gg/kn/knowledge/util"
	"gitlab.com/rwx.gg/kn/knowledge/util/cache"
)

// OpenBase is the main function of this API. It opens a new knowledge
// base from a given directory and prepares internal caching to later
// Cache Node information.
func OpenBase(d string) (Base, error) {
	var err error
	b := new(base)
	b.cache = cache.New()
	if b.path, err = filepath.Abs(d); err != nil {
		return nil, err
	}
	b.pathl = len(b.path)
	return b, nil
}

// -------------------------------------------------------------------

// Base internally caches the information about the knowledge base and
// all its Nodes as it Opens each one. The underlying Component YAML
// will be parsed but the Markdown body (MD) will not to conserve
// memory. Subsequent calls to Open will return the cached Node
// reference until the cache is cleared.
type Base interface {
	DataDoc                        // Base embeds its own DataDoc itself
	cache.Interface                // Cache, Recache, ClearCache
	Source                         // (S) URL to source repo
	Open(ref string) (Node, error) // opens, caches, and returns a Node
}

// Source is the full URL to the repository containing the source of
// this knowledge base. Usually this will be a Git hosting service but
// is not required to be such.
type Source interface{ Source() string }

// -------------------------------------------------------------------

type base struct {
	datadoc
	cache        cache.Accessible // delegate, not embed
	source       string
	fmt.Stringer // fulfilled as JSON
}

func (b *base) Source() string    { return b.source }
func (b *base) String() string    { return util.JSON(b) }
func (b *base) ClearCache() error { b.cache.Empty(); return nil }
func (b *base) Recache() error    { b.cache.Empty(); return b.Cache() }

func (b *base) Open(id string) (Node, error) {
	n := b.cache.Get(id).(*node)
	if n != nil {
		return nil, fmt.Errorf("Node not found: %v", id)
	}
	n = new(node)
	n.id = id
	n.path = filepath.Join(b.path, id)
	n.md = n.PathTo(`README.md`)
	if !n.Has(`README.md`) {
		return nil, fmt.Errorf("No README.md found: %v", id)
	}
	line := util.FirstLineWithPrefix(n.md, "Type: ", 6)
	if len(line) > 0 {
		i, e := strconv.Atoi(line[6:])
		if e != nil {
			return nil, e
		}
		n.typ = i
	}
	b.cache.Set(id, n)
	return n, nil
}

// Cache visits every subdirectory within the knowledge base to discover
// nodes with a README.md file and adds them to the internal cache by
// calling b.Node(ref). Fulfills the missing cache.Cache method from
// cache.Struct.
//
func (b *base) Cache() error {
	cl := func(p string, f os.FileInfo, err error) error {
		name := f.Name()
		if f.IsDir() && (name[0] == '.' || name[0] == '_') {
			return filepath.SkipDir
		}
		if name == `README.md` {
			ref := p[b.pathl+1:]
			if ref != "" {
				_, e := b.Open(ref[:len(ref)-10])
				return e
			}
		}
		return nil
	}
	return filepath.Walk(b.path, cl)
}

/*
// NodesLastChanged returns the list of Nodes sorted by newest to
// oldest.
//
func (b *base) NodesLastChanged() ([]*Node, error) {
	if err := b.CacheIfNeeded(); err != nil {
		return nil, err
	}
	nodes := b.nodes // fresh copy (same refs) to not mess with internal
	sort.Slice(nodes, func(i, j int) bool {
		return nodes[j].ModTime.After(nodes[i].ModTime)
	})
	return nodes, nil
}

// MakeManifest generates the required MANIFEST file in the
// Base.Dir. The internal nodes cache will be used if found.
//
func (b *Base) MakeManifest() error {
	if err := b.CacheIfNeeded(); err != nil {
		return err
	}
	f, err := os.OpenFile(b.File(`MANIFEST`),
		os.O_RDWR|os.O_CREATE, 0644)
	defer f.Close()
	if err != nil {
		return err
	}
	nodes, err := b.NodesLastChanged()
	if err != nil {
		return err
	}
	for _, n := range nodes {
		fmt.Fprintf(f, "%v %v\n", n.ModTime.Unix(), n.Ref)
	}
	return nil
}

// CacheIfNeeded caches if the knowledge base has never been fully cached.
//
func (b *Base) CacheIfNeeded() error {
	if !b.cache.full {
		return b.Cache()
	}
	return nil
}
*/
