package knowledge

import (
	"fmt"
	"time"

	"gitlab.com/rwx.gg/kn/knowledge/util"
)

// DataDoc is the main knowledge Node type. It includes the traditional
// form of a README.md document with optional assets files in the same
// directory used by the README.md (images, etc.) but also adds support
// for optional structured data rendered inline through the
// use of special [DATA](file.json) one-line paragraph blocks within the
// README.md file that are replaced with rendered data.
type DataDoc interface {
	Node
	Title        // (T) highly relevant title (see Query)
	Subtitle     // (t) secondarily relevant subtitle
	Summary      // (S) short paragraph of TLDR
	Headings     // (H) all section headings
	SeeAlso      // (A) list of suggested refs, local and external
	Created      // (C) creator and time of creation
	Updates      // (U) list of significant updates
	Tags         // (G) list of tags
	Contribs     // (R) defaults to Base, list of contributors
	Query        // (Q) title doubles as a search query
	WIP          // (W) work in progress, might not want yet
	fmt.Stringer // fulfilled as JSON
}

// Title returns the title string. Titles must be one line string of
// emphasis-only Pandoc Markdown (links are stripped, for example).
// Every DataDoc is required to have a Title.
type Title interface{ Title() string }

// Subtitle returns the subtitle string. Subtitle is exactly like Title
// except it should contain secondary information. Subtitles are always
// optional.
type Subtitle interface{ Subtitle() string }

// Summary is the TL;DR; version that must convey the main point and
// conclusion within a short paragraph. Summary YAML is optional but
// will be assumed to be the first paragraph if omitted. Template
// creators can decide whether to display the Summary in the rendered
// output or not. If so labeling it "Summary", "Abstract", or "TL;DR;"
// is common.  While there is no length limit 140 characters (2 lines)
// is ideal and 360 (5 lines) is pushing it.
type Summary interface{ Summary() string }

// Headings contains the level (L), identifier (I), and Pandoc Markdown
// text (T) and are included in the main JSON metadata file of the
// knowledge Base to facilitate searching. Links will be stripped.
type Heading struct {
	L int    // 2-6
	I string // autoidentifier
	T string // Pandoc Markdown (emphasis-only)
}

// Headings return all Pandoc Markdown headings in the document in the
// order that they are encountered. Links and attributes are stripped
// but if an identifier is detected it will override the autoidentifier
// created from the heading text. Autoidentifers use the strictly
// specified Pandoc autoidentifier algorithm. Level one Headings are
// strongly discouraged since they usually conflict with the rendered
// Title (level one).
type Headings interface{ Headings() []Heading }

// Reference is text with an optional URL that will be linked to the
// text in render formats where such is possible. Otherwise it will
// simply be printed as text as well. The URL is frequently omitted for
// more traditional styles of written references from simple
// descriptions to formal bibliography entries. If a node identifier (N)
// is provided links will be rendered with wrapping slashes as needed
// (/some/node/).
type Reference struct {
	T string // optional text or title of reference
	U string // universal resource locator
	N string // node identifier (some/node)
}

// SeeAlso refers to other supporting and opposing content locally or
// externally. Each entry is a Pandoc Markdown paragraph. SeeAlso
// content is usually rendered with the Body content before any footer
// but depends entirely on render templates.
type SeeAlso interface{ SeeAlso() []string }

// Created tells who created the content (Creator) and when (Created).
// The Creator is implied to be that of the root knowledge node when
// omitted. The Creator string is expected to follow the standard
// division into name, <email>, and (web/social) spans. Time must always
// be an ISO date-time.
type Created interface {
	Creator() string    // Some Body <somebody@pm.me> (some.dev)
	Created() time.Time // ex: 2020-07-06T17:06:12-0400
}

// Update contains the time (T) and reason (Y) for the update.
type Update struct {
	T time.Time
	Y string
}

// Updates contain a history of every significant update.
type Updates interface{ Updates() []Update }

// Tags contains no-space keywords akin to those found in social media.
// While there is no limit, five is preferable as every tag will
// usually be rendered with the body content.
type Tags interface{ Tags() []string }

// Contribs contains every contributor listed in the same style as
// Created.Creator.
type Contribs interface{ Tags() []string }

// Indicates that the title can also serve as an Internet search query.
// Usually this is rendered as a hyperlinked query to a responsible
// search engine.
type Query interface{ Query() bool }

// WIP indicates the component is a work in progress and can possibly be
// ignored or consumed with the understanding it will soon change.
type WIP interface{ WIP() bool }

// -------------------------------------------------------------------

type datadoc struct {
	node
	title    string
	subtitle string
	summary  string
	created  time.Time
	creator  string
	contribs []string
	updates  []Update
	tags     []string
	headings []Heading
	seealso  []string
	query    bool
	wip      bool
}

func (d *datadoc) Title() string       { return d.title }
func (d *datadoc) Subtitle() string    { return d.subtitle }
func (d *datadoc) Summary() string     { return d.summary }
func (d *datadoc) Headings() []Heading { return d.headings }
func (d *datadoc) SeeAlso() []string   { return d.seealso }
func (d *datadoc) Created() time.Time  { return d.created }
func (d *datadoc) Creator() string     { return d.creator }
func (d *datadoc) Updates() []Update   { return d.updates }
func (d *datadoc) Tags() []string      { return d.tags }
func (d *datadoc) Contribs() []string  { return d.contribs }
func (d *datadoc) Query() bool         { return d.query }
func (d *datadoc) WIP() bool           { return d.wip }
func (d *datadoc) String() string      { return util.JSON(d) }
