package util

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_FileContains(t *testing.T) {
	require.True(t, FileContains("../testing/kbase1/something/README.md", "Something"))
}

func Test_FileExists(t *testing.T) {
	require.True(t, FileExists("../testing"))
	require.False(t, FileExists("../testting"))
}

func Test_ParentDirWith(t *testing.T) {
	kn1 := ParentDirWith(".kn", "../testing/kbase1/something")
	if !strings.HasSuffix(kn1, "testing/kbase1/.kn") {
		t.Fail()
	}
}

func Test_FirstLineWithPrefix(t *testing.T) {
	line := FirstLineWithPrefix("../testing/kbase1/something/README.md", "Type:", 2)
	require.Equal(t, "", line)
	line = FirstLineWithPrefix("../testing/kbase1/something/README.md", "Type:", 10)
	require.Equal(t, "Type: article", line)
}
