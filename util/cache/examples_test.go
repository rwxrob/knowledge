package cache_test

import (
	"fmt"
	"html/template"
	"os"
	"sync"
	"time"

	"gitlab.com/rwx.gg/kn/knowledge/util/cache"
)

func ExampleNew() {
	c := cache.New()
	fmt.Println(c)

	// Output:
	// {}
}

func ExampleStruct_Empty() {
	// re-initialize
	d := cache.New()
	d.Set("some", "thing")
	fmt.Println(d)
	d.Empty()
	fmt.Println(d)

	// Output:
	// {"some":"thing"}
	// {}
}

func ExampleStruct() {
	c := cache.New()
	c.Set("some", "thing")
	c.Set("number", 42)
	fmt.Println(c.Count())

	// Output:
	// 2
}

func ExampleStruct_Set() {
	c := cache.New()
	c.Set("some", "thing")
	c.Set("number", 42)

	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		c.Set("concurrency", "safe")
		wg.Done()
	}()
	wg.Wait()

	fmt.Println(c.Has("some"))
	fmt.Println(c.Has("number"))
	fmt.Println(c.Has("blah"))

	fmt.Println(c.Get("some"))
	fmt.Println(c.Get("number"))
	fmt.Println(c.Get("concurrency"))

	// Output:
	// true
	// true
	// false
	// thing
	// 42
	// safe
}

func ExampleStruct_SetFull() {
	c := cache.New()
	fmt.Println(c.LastFull())
	fmt.Println(c.IsFull())
	c.SetFull(true)
	fmt.Println(c.LastFull().Day() == time.Now().Day())
	fmt.Println(c.IsFull())

	// Output:
	// 0001-01-01 00:00:00 +0000 UTC
	// false
	// true
	// true
}

func ExampleStruct_String() {
	c := cache.New()
	c.Set("some", "thing")
	fmt.Println(c)

	// Output:
	// {"some":"thing"}
}

func ExampleStruct_For() {
	c := cache.New()
	c.Set("some", "thing")
	c.Set("another", "one")
	c.Set("num", 42)

	action := func(item interface{}) error {
		fmt.Printf("%v\n", item)
		c.Set("boo", item) // should be safe for concurrency
		return nil
	}

	c.For(action, 0, nil)

	// Unordered output:
	// thing
	// one
	// 42
}

func ExampleStruct_For_template() {
	c := cache.New()

	type Person struct {
		Name string
		Age  int
	}

	c.Set("Rob", Person{"Rob", 52})
	c.Set("Other", Person{"Person", 19})

	tmpl, _ := template.New("out").Parse("Name:  {{.Name}}\nOther: {{.Age}}\n")

	action := func(item interface{}) error {
		return tmpl.Execute(os.Stdout, item)
	}

	c.For(action, 0, nil)

	// Unordered output:
	// Name:  Rob
	// Other: 52
	// Name:  Person
	// Other: 19
}

func ExampleStruct_For_async() {
	c := cache.New()
	c.Set("some", "thing")
	c.Set("another", "one")
	c.Set("num", 42)

	action := func(item interface{}) error {
		fmt.Printf("%v\n", item)
		c.Set("boo", item) // should be safe for concurrency
		return nil
	}

	c.For(action, 3, nil)

	// Unordered output:
	// thing
	// one
	// 42
}

func ExampleStruct_For_chan() {
	c := cache.New()
	c.Set("some", "thing")
	c.Set("another", "one")
	c.Set("num", 42)

	action := func(item interface{}) error {
		fmt.Printf("%v\n", item)
		c.Set("boo", item) // should be safe for concurrency
		return nil
	}

	rvals := make(chan error, c.Count())

	c.For(action, 0, rvals)

	for r := range rvals {
		fmt.Println(r)
	}

	// Unordered output:
	// thing
	// one
	// 42
	// <nil>
	// <nil>
	// <nil>
}

func ExampleStruct_For_async_chan() {
	c := cache.New()
	c.Set("some", "thing")
	c.Set("another", "one")
	c.Set("num", 42)

	action := func(item interface{}) error {
		fmt.Printf("%v\n", item)
		c.Set("boo", item) // should be safe for concurrency
		return nil
	}

	rvals := make(chan error, c.Count())

	c.For(action, 3, rvals)

	for r := range rvals {
		fmt.Println(r)
	}

	// Unordered output:
	// thing
	// one
	// 42
	// <nil>
	// <nil>
	// <nil>
}
