package cache

import (
	"encoding/json"
	"sync"
	"time"
)

// Interface provides the minimum required methods any struct must
// provide to qualify at a cache. The internals of how the cache is
// managed are ignored and left up to the internal methods to provide.
// For more design with more direct manipulation of a cache use
// Accessible instead.
type Interface interface {
	Cache() error      // only if not already cached
	Recache() error    // force to cache everything again
	ClearCache() error // keeps cache but clears fully
}

// Accessible provides the interface to something that is fully
// accessible through accessors and mutators, can be filled, and allows
// synchronous and asynchronous actions to be performed on each item.
type Accessible interface {
	Accessors
	Fillable
	For
}

// Accessors interface integrates an accessor/adder/mutator way of
// updating its internals.
type Accessors interface {
	Set(k string, v interface{}) // simple set
	Get(k string) interface{}
	Has(k string) bool
}

// Full interface helps keep track of things that can be filled. Empty
// must completely empty the item to a fully initialized state as if it
// were new. The filling is left up to other methods.
type Fillable interface {
	SetFull(bool)        // changes full state, updates LastFull
	IsFull() bool        // is this thing full?
	LastFull() time.Time // last time SetFull true
	Count() int          // how full is this thing?
	Empty()              // clears to absolute empty
}

// For performs the specified Action on a set of implicit items within
// the struct. If the limit is 0 will do so one item at a time. If the
// limit it greater than 0 then actions will be processes at the same
// time producing up to the limit number of goroutines. If the optional
// return values channel is passed will send the return values to it as
// each is completed.
type For interface {
	For(act Action, lim int, rvals chan error)
}

// Action is a function that will be passed the item from the cache and
// must return an error. A single Action is passed to the For method
type Action func(item interface{}) error

// ----------------------------------------------------------------

// Struct fulfills all but one of the interfaces of a cache.Accessible
// (essentially producing traits). The Cache interface (which is usually
// highly specialized) has been left out. It is common to embed Struct
// and fulfill the Cache interface by calling other methods from the
// Accessible interface.
type Struct struct {
	Accessible

	items    []interface{}
	index    map[string]interface{}
	length   int
	full     bool
	lastfull time.Time
	mutex    sync.Mutex
}

func New() Accessible {
	s := new(Struct)
	s.Init()
	return s
}

func (s *Struct) Init() {
	s.items = []interface{}{}
	s.index = map[string]interface{}{}
	s.full = false
	s.lastfull = time.Time{}
	return
}

func (c *Struct) Set(key string, item interface{}) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.index[key] = item
	c.items = append(c.items, item)
}

func (c *Struct) Get(key string) interface{} {
	k, _ := c.index[key]
	return k
}

func (c *Struct) Has(key string) bool {
	_, has := c.index[key]
	return has
}

func (c *Struct) SetFull(nw bool) {
	c.full = nw
	c.lastfull = time.Now()
}

func (c *Struct) IsFull() bool        { return c.full }
func (c *Struct) LastFull() time.Time { return c.lastfull }
func (c *Struct) Count() int          { return len(c.items) }
func (c *Struct) Empty()              { c.Init() }
func (c *Struct) String() string      { return c.JSON() }

func (c *Struct) Recache() { c.Init() }

func (c *Struct) JSON() string {
	byt, e := json.Marshal(c.index)
	if e != nil {
		return ""
	}
	return string(byt)
}

// For runs the given action on each cached item. By default each item
// is handled sequentially. If the second argument is greater than
// 1 then a number of goroutines up to that limit will be created and
// the action on each will be asynchronous and in no particular
// sequence.  The third parameter is a channel that will be sent the
// return values of each action error return value as completes. After
// all have completed the channel will be closed. Passing a buffered
// channel with less than Count (number of items) will block until the
// return values are read.  Block on return value reads does not block
// the processing of actions on items in any way.  If the channel passed
// is nil error results will be ignored but the actions will still run.
// If the action is nil a panic will occur.
//
// Note that the concurrent goroutine limit can be rather high.
// Goroutines are very light weight and, according to the docs, "it is
// practical to create hundreds of thousands of goroutines in the same
// address space." This depends on what the action is, however. Forking
// a subprocess command (such as Pandoc) could require a lower limit
// but, for most modern devices, is still probably in the thousands. On
// the other hand, if you know you don't need a high degree of
// concurrency you can keep the limit rather small and reduce the slight
// overhead of setting up the internal concurrency channel semaphore.
// Benchmark testing can help determine the best limit for a particular
// action more objectively.
//
func (c *Struct) For(act Action, lim int, rvals chan error) {
	items := c.items
	// (act, 0, nil)
	if lim == 0 {
		if rvals == nil {
			for _, item := range items {
				act(item)
			}
			return
		}
		// (act, 0, rvals)
		for _, item := range items {
			rvals <- act(item)
		}
		close(rvals)
		return
	}
	// limited channel semaphore idiom (from net pkg)
	sem := make(chan interface{}, lim)
	for _, item := range items {
		sem <- true // blocks if full
		// (act, 3, nil)
		if rvals == nil {
			go func(item interface{}) {
				defer func() { <-sem }() // frees
				act(item)
			}(item)
			continue
		}
		// (act, 3, rvals)
		go func(item interface{}) {
			defer func() { <-sem }() // frees
			rvals <- act(item)
		}(item)
	}
	// waits for all (keeps filling until full again)
	for i := 0; i < cap(sem); i++ {
		sem <- true
	}
	// all goroutines have now finished
	if rvals != nil {
		close(rvals)
	}
}
