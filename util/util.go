package util

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// FirstLineWithPrefix scans the file at path checking each line as it
// goes for the given prefix. If the limit is reached ends the scan.
// This is the fastest and most memory efficient way to scan a file.
// Returns an empty string if not found or limit was reached.
//
func FirstLineWithPrefix(path, pre string, lim int) string {
	file, _ := os.Open(path)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for i := 0; i < lim; i++ {
		if !scanner.Scan() {
			return ""
		}
		txt := scanner.Text()
		tlen := len(txt)
		if tlen >= len(pre) && pre == txt[:len(pre)] {
			return txt
		}
	}
	return ""
}

// FileContains loads the entire file at path and checks for the
// existence of the given string. Consider others when memory and/or
// large files are a concern.
//
func FileContains(path, s string) bool {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return false
	}
	return strings.Contains(string(buf), s)
}

// FileExists checks for anything that exists at passed location and
// returns true if found. If does not exist -- or could not determine
// that it exists -- false is returned. Note file may actually exist and
// just not be visible to the caller due to permissions.
//
func FileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

// ParentDirWith looks in the start directory for anything with the given
// name and if not found progresses up the directory tree to the parent
// looking there recursively until it is found and returns it. If
// nothing is found an empty string is returned.
//
func ParentDirWith(name string, dir string) string {
	dir, _ = filepath.Abs(dir)
	if dir == "/" {
		return ""
	}
	path := filepath.Join(dir, name)
	if FileExists(path) {
		return path
	}
	return ParentDirWith(name, filepath.Dir(dir))
}

// JSON returns the thing converted into JSON.
func JSON(thing interface{}) string {
	byt, e := json.Marshal(thing)
	if e != nil {
		return ""
	}
	return string(byt)
}

// Dump prints the thing as compressed JSON.
func Dump(thing interface{}) { fmt.Println(JSON(thing)) }

// RunCommand runs the command represented by the passed arguments exactly
// as if typed into most shell interpreters. The standard input, output,
// and error are connected directly to that of the OS. Unlike other
// Command and Exec variations the first argument is assumed to be the
// name of the command to executed. Local and absolute commands can be
// used. Local commands will be looked up in the system path and produce
// an error if it does not exist. Command is safe for use on all
// operating systems (no Unix dependencies).
//
func RunCommand(args ...string) error {
	path, err := exec.LookPath(args[0])
	if err != nil {
		return err
	}
	cmd := exec.Command(path, args[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
