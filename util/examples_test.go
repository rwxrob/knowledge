package util_test

import "gitlab.com/rwx.gg/kn/knowledge/util"

func ExampleDump() {
	d := []interface{}{42, "something"}
	util.Dump(d)

	// Output:
	// [42,"something"]
}
