# Bash Prototype Code

Various Bash scripts created over the years. The functionality of the [README.World Knowledge Management Utility](../) has come from more than six years of different quick-and-dirty shell prototypes that have generally been good enough to get the job done. 

