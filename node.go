package knowledge

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/rwx.gg/kn/knowledge/util"
)

// Node is an embedded Component that knows which Base it belongs to and
// where it exists in the local filesystem. Nodes can easily be scanned
// without fully parsing the README.md YAML and Markdown of the
// Component. Usually developers should embed Node and not Component
// depending on the situation.
type Node interface {
	Component    // YAML and MD
	Type         // (Y)
	Ref          // rooted to Base.Path with slashes (/foo/)
	Path         // absolute host system path to Base directory
	PathTo       // absolute path to a given file or directory
	Has          // contains given file or directory?
	README       // absolute host system path to README.md file
	FileInfo     // last read README.md FileInfo with ModTime
	fmt.Stringer // fulfilled as JSON
}

// Type indicates the component type. Values within -99 < val < 99 are
// reserved for the permanent specification of built-in types. DataDoc
// (0) is the default type when the Type YAML property is omitted or not
// found within the required first six lines of the README.md file.
// Negative Type numbers indicate dynamically generated from static and
// other dynamic component types.
//
// Creation of additional types is strongly discouraged -- particularly
// for static components (>1). The Knowledge Net depends on consistent
// knowledge source syntax in much the same way that the Web depends on
// HTML. DataDoc provides a flexible way to incorporate all other data
// formats and structures while maintaining a consistent syntax
// standard.
type Type interface{ Type() int }

// Ref returns a rooted directory of a Node. Refs must always begin and
// end with a slash (/some/foo/) when they appear in the README.md file.
// A Ref without the beginning and trailing slash is referred to as
// an ID instead (some/foo) and is useful for storing the least amount
// of characters.
type Ref interface {
	Ref() string // /some/foo/
	ID() string  // some/foo
}

// Path is for things that know their own absolute directory path.
type Path interface {
	Path() string
}

// README is the absolute path to the README.md file.
type README interface {
	README() string
}

// FileInfo returns the last cached os.FileInfo for the README.md file.
type FileInfo interface {
	FileInfo() os.FileInfo
}

// Has returns true if the relative path exists.
type Has interface {
	Has(rel string) bool
}

// PathTo returns the absolute path to the relative path passed.
type PathTo interface {
	PathTo(rel string) string
}

// -------------------------------------------------------------------

type node struct {
	component
	path  string
	pathl int
	id    string
	md    string
	i     os.FileInfo
}

func (n *node) Ref() string           { return "/" + n.id + "/" }
func (n *node) ID() string            { return n.id }
func (n *node) Path() string          { return n.path }
func (n *node) README() string        { return n.md }
func (n *node) FileInfo() os.FileInfo { return n.i }
func (n *node) String() string        { return util.JSON(n) }

func (n *node) PathTo(r string) string {
	return filepath.Join(n.path, r)
}

func (n *node) Has(r string) bool {
	return util.FileExists(n.PathTo(r))
}
