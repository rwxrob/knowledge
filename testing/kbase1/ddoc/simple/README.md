---
Title: Test of DataDoc with Single Data File
---

Here is the summary and first paragraph. The ~~DATA~~ section to be
replaced must be on its own line with zero to three initial spaces (four
or more makes it into a verbatim block.

   ~~DATA~~

I think the use of strikethrough really works because it indicates that
would be replaced with the data and that the DATA word would not really
be there.

Another paragraph here.

