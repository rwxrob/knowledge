---
Title: Test of DataDoc with Multiple Data Files
---

  [DATA Some Other Name](now.md)

And here is another one that is just essentially an *include*.

  [DATA](another.md)

That one would normally have appeared before the other if only `DATA`
was used and the order of files in `data` decided which appeared first.

