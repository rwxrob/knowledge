# RWX Knowledge Net Domain Model and Reference API in Golang

[![WIP](https://img.shields.io/badge/status-wip-red)](#work-in-progress)
[![Go Report
Card](https://goreportcard.com/badge/gitlab.com/rwx.gg/kn/knowledge)](https://goreportcard.com/report/gitlab.com/rwx.gg/kn/knowledge)
[![Coverage](https://gocover.io/_badge/gitlab.com/rwx.gg/kn/knowledge)](https://gocover.io/gitlab.com/rwx.gg/kn/knowledge)
[![GoDoc](https://godoc.org/gitlab.com/rwx.gg/kn/knowledge?status.svg)](https://godoc.org/gitlab.com/rwx.gg/kn/knowledge)


## Design Considerations

* Simplest possible solutions
* Convention over configuration
* Coding by contract
* SOLID design (interfaces over structs)
* Render and build from files only (no fetching)
* Separation of concerns
* Decoupled data files to enable external fetching
* Hard coded YAML and limited Pandoc Markdown into API
