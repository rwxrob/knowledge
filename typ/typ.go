/*
Package typ contains the reserved Node.Type integer constants as
a reference and summary.

*/
package typ

const (

	// dynamic, generated from other nodes
	TITLES    int = -iota // flat title list, different sorting options
	HEADINGS              // titles with headings at depth
	SUMMARIES             // node summaries
	ABRIDGED              // shortened verion of compiled, by depth
	COMPILED              // list of nodes rendered inline, books, etc.

	// administrative lists by title with link
	NEWEST   int = iota - 10 // titles in manifest order, top-able
	OLDEST                   // titles in rev manifest order, top-able
	MOVED                    // just perm redirects
	SHORTCUT                 // just convenience redirects
	ORPHAN                   // nothing linking to them
	BROKEN                   // outbound links fail
	INVALID                  // contain invalid content per spec
	MISTAKE                  // spelling, grammar mistakes

	// maps usually displayed graphically
	DEPSMAP int = -iota + 80 // node and external dependencies with counts
	NODEMAP                  // only internal node dependencies
	WORDMAP                  // word usage counts from dex
	SUBSMAP                  // weighted subs data by network level

	// min reserved is -99

	// static and semantic, consistent content organization and data
	DataDoc  int = iota // default
	BaseRoot            // root node of a knowledge base containing nodes
	Def                 // single definition, term, concept, proper noun
	Log                 // headings are 2nd level date and times
	Article             // traditional article, blog post
	Chapter             // traditional book chapter
	Paper               // formal academic paper
	Review              // extension of article with criteria and ratings
	HowTo               // 2nd levels automatically numbered as steps
	Spec                // requirements, pre/post conditions, test cases
	Chal                // Spec and HowTo with performance history log

	// max reserved is 99
)
