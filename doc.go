// Copyright 2020 Rob Muhlestein.
// Use of this source code is governed by the GPLv2 which
// can be found in the LICENSE file within parent directory.

/*
Package knowledge provides the domain model and reference API for the
RWX Knowledge Net specification.
*/
package knowledge
