module gitlab.com/rwx.gg/kn/knowledge

go 1.14

require (
	github.com/stretchr/testify v1.6.1
	gitlab.com/rwx.gg/async v0.0.0-20200629003030-d11c14926f79
	gitlab.com/rwx.gg/cmdtab v0.0.0-20200629192901-ffca3b74ad33
)

// TODO remove replace before prod release
replace gitlab.com/rwx.gg/async => ../../async
