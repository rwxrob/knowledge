package knowledge

import "gitlab.com/rwx.gg/kn/knowledge/util"

// A Component is the fundamental, embeddable component of all knowledge
// Nodes. A Component must only exist as a filesystem directory that
// contains a required README.md file with other optional resource files
// (images, etc.) linked from the README.md file. Any orphan files
// invalidate the Component and stop it from opening and rendering.
type Component interface {
	YAML // parsed YAML data map[string]interface{}
	MD   // raw limited Pandoc Markdown string content
}

// YAML is the fundamental syntax of structured data on the Knowledge
// Net. Therefore, every Component README.md file must begin immediately
// with a YAML header. Unlike other systems the YAML header is
// mandatory. YAML uses the same beginning and ending marker of a line
// with nothing but three dashes (---).
//
// [Use of the common term "front-matter" (which implies optional data
// in multiple possible formats) is strongly discouraged. Please use
// "YAML header", "YAML data" or just "YAML" or "header" instead.]
type YAML interface{ YAML() map[string]interface{} }

// MD contains the Pandoc Markdown body that begins immediately after
// the first blank line after the YAML header (---). This extra blank
// line provides better readability and addresses problems with many
// broken parsers. The content itself must be Pandoc Markdown as
// described in the RWX Knowledge Net specification.
type MD interface{ MD() string }

// -----------------------------------------------------------------

type component struct {
	typ int
	syn string
	yml map[string]interface{}
	md  string
}

func (c *component) Type() int                    { return c.typ }
func (c *component) YAML() map[string]interface{} { return c.yml }
func (c *component) MD() string                   { return c.md }
func (c *component) String() string               { return util.JSON(c) }
