package block

const (
	BLOCK = 0

	_text = iota
	TEXT  // verbatim text
	MARK  // single paragraph of extended commonmark
	CODE  // syntax highlighted code of any kind
	_text_end

	_list     = iota + 100
	LIST      // yaml one-level, unordered, bullets
	NUMLIST   // yaml one-level, ordered, numbered
	PARALIST  // yaml paragraph list, topical + body
	UNORDERED // commonmark unordered list
	ORDERED   // commonmark ordered list
	URLS      // yaml list URLs with titles
	REQS      // yaml list of requirements, one to a line
	LOG       // yaml list of timestamped past entries, one paragraph each
	SCHED     // yaml list of date-time future events, one paragraph each
	_list_end

	_data   = iota + 200
	TABLE   // yaml one line per record
	RECORDS // table in long form with named feilds
	_data_end

	_chart = iota + 300
	CHART  // yaml table data rendered as standard chart types
	BAR
	PIE
	GANT
	_chart_end

	_image = iota + 400
	SVG    // standard vector graphic
	RASTER // any raster images
	JPG    // specifically JPEG format
	GIF    // specifically GIF format
	PNG    // specifically PNG format
	_image_end

	_video   = iota + 500
	VIDEOURL // link to remote video, usually with thumbnail
	VIDEO    // local video
	MP4      // specifically mp4 local video
	MOV      // specifically mov local video
	AVI      // specifically avi local video
	_video_end

	_audio = iota + 600
	AUDIO  // yaml rendered as inline or inline thumbnail link to audio
	MP3
	WAV
	_audio_end

	// rendered blocks are those that need some form of rendering beyond
	// standard commonmark rendering

	_rendered = iota + 1000
	HTML      // any valid html5
	LATEX     // LaTeX partial
	MATHML    // mathml patrial
	_rendered_end

	GROUP = 9000 // group of any blocks or other groups
)
