package knowledge

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNode_File(t *testing.T) {
	origd, _ := os.Getwd()
	b, _ := Open("testing/kbase1")
	n, _ := b.Node("something")
	require.Equal(t,
		n.PathTo("README.md"),
		filepath.Join(origd, "testing/kbase1", "something", "README.md"),
	)
}

func TestNode_HasFile(t *testing.T) {
	b, _ := Open("testing/kbase1")
	n, _ := b.Node("something")
	require.True(t, n.Has("README.md"))

}
