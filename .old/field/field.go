package field

const (
	_text = iota
	TEXT  // plain text with no implied format or meaning
	NAME  // person or thing, first and last, nick, etc.
	_text_end

	_mark = iota + 100
	MARK  // text with CommonMark emphasis only
	TITLE // one line title or subtitle, CommonMark emphasis only
	// no HEADING because just a title of something else
	_mark_end

	_measure = iota + 200
	AMOUNT   // float64 measure of anything
	_measure_end

	_time    = iota + 300
	UNIX     // seconds (with fractional) since epoch
	TSTAMP   // 2020-07-06T11:29:29-0400
	YEAR     // 1-12
	MONTH    // 1-12
	YEARDAY  // 1-366
	MONTHDAY // 1-32
	WEEKDAY  // 0-7 (Sunday = 0)
	HOUR     // 0-23
	MIN      // 0-59
	SEC      // 0-59 (optional fractional as well)
	_time_end

	_location = iota + 400
	PLACE     // location as plain text
	GEOLOC    // <lat><space><long>
	ADDR      // all of address on single line of text
	ADDRESS1  // first line of address
	ADDRESS2  // first line of address
	POSTAL    // zip or other postal code
	REGION    // county, province, etc.
	STATE     // US listing of states
	COUNTRY   // international country code
	_location_end

	_internet = iota + 500
	URL       // a way to locate something, any protocol
	EMAIL     // standard email address
	DOMAIN    // top-level domain with optional subdomain
	ACCOUNT   // name of an account on a given service
	CONTACT   // Some One Here <some@email.com> (myweb.com)
	_internet_end
)
