package knowledge

// Log contains entries for keeping track of things.
type Log struct {
	Entries []Entry
}

// Max returns the highest Amount.
func (log *Log) Max() float64 {
	max := log.Entries[0].Amount()
	for _, e := range log.Entries {
		a := e.Amount()
		if a > max {
			max = a
		}
	}
	return max
}

// Min returns the lowest amount.
func (log *Log) Min() float64 {
	min := log.Entries[0].Amount()
	for _, e := range log.Entries {
		a := e.Amount()
		if a < min {
			min = a
		}
	}
	return min
}

// Average returns the average from all entry amounts.
func (log *Log) Average() float64 {
	a := 0.0
	for _, e := range log.Entries {
		a += e.Amount()
	}
	return a / float64(len(log.Entries))
}
