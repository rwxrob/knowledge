package comp

const (
	COMPONENT int = 0 // default

	// ---------------------- documents (stand-alone) -------------------

	_doc    int = iota
	DEF         // definition of thing in title, alts in subtitle
	POST        // creator and created date, one paragraph or so
	QUOTE       // who, when, where, commonmark
	ARTICLE     // art-directed blog post, magazine article, etc.
	CHAPTER     // un-numbered book chapter
	SPEC        // specification with REQS, user stories, whatever
	HOW         // step by step walthrough usually with conceal, tutorial
	TODO        // light-weight todo list with prioritization
	_doc_end

	// -------------------------- administrative ------------------------

	_admin   = iota + 400
	REDIRECT // former location or another component, temporary
	SHORTCUT // permanent alternative location that redirects
	_admin_end

	// --------------------------- applications  ------------------------

	_app int = iota + 1000
	APP      // any app that takes over device or modal window fully
	_app_end
)
