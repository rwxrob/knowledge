package knowledge

import (
	"os"
	"path/filepath"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBase_NewBase(t *testing.T) {
	origd, _ := os.Getwd()
	b, err := NewBase("testing/kbase1")
	require.Nil(t, err)
	require.NotNil(t, b)
	require.Equal(t, filepath.Join(origd, "testing/kbase1"), b.Dir)
}

func TestBase_CacheNodes(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	b.Cache()
	require.Equal(t, "foo/bar", b.nodes[0].Ref)
	require.Equal(t, "something", b.nodes[1].Ref)
}

func TestBase_Walk(t *testing.T) {
	origd, _ := os.Getwd()
	b, err := NewBase("testing/kbase1")
	require.Nil(t, err)
	stuff := []string{}
	b.Walk(func(b *Base, path string) error {
		stuff = append(stuff, path)
		return nil
	})
	require.Equal(t, filepath.Join(origd, "testing/kbase1"), stuff[0])
	require.Equal(t, filepath.Join(origd, "testing/kbase1/MANIFEST"), stuff[1])
}

func TestBase_NodesLastChanged(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	n, _ := b.NodesLastChanged()
	require.Equal(t, n[0].Ref, "foo/bar")
}

/*
func TestBase_MakeManifest(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	manifest := "testing/kbase1/MANIFEST"
	os.Remove(manifest)
	b.MakeManifest()
	buf, err := ioutil.ReadFile(manifest)
	require.Nil(t, err)
	require.Contains(t, string(buf), "something")
}
*/

func TestBase_File(t *testing.T) {
	origd, _ := os.Getwd()
	b, _ := NewBase("testing/kbase1")
	require.Equal(t,
		b.File("something", "README.md"),
		filepath.Join(origd, "testing/kbase1", "something", "README.md"),
	)
}

func TestBase_HasFile(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	require.True(t, b.HasFile("something", "README.md"))
	require.True(t, b.HasFile("something/README.md"))
}

func TestBase_ForEach(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	stuff := map[string]bool{}
	err := b.ForEach(func(n *Node) error {
		stuff[n.Ref] = true
		return nil
	})
	require.Nil(t, err)
	want := map[string]bool{
		"foo/bar":   true,
		"something": true,
		"vid":       true,
	}
	require.Equal(t, want, stuff)
}

func TestBase_ForEachAsync(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	stuff := map[string]bool{}
	mut := new(sync.Mutex)
	errs := b.ForEachAsync(3, func(n *Node) error {
		mut.Lock()
		stuff[n.Ref] = true
		mut.Unlock()
		return nil
	})
	require.Nil(t, errs)
	want := map[string]bool{
		"foo/bar":   true,
		"something": true,
		"vid":       true,
	}
	require.Equal(t, want, stuff)
}

func TestBase_Node(t *testing.T) {
	b, _ := NewBase("testing/kbase1")
	n, _ := b.Node("something")
	require.Equal(t, DOCUMENT, n.Type)
}
