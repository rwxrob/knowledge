package knowledge

// This file contains the common Component interface types that are
// basic building blocks used by other higher-level knowledge Components
// contained in their own files.

import "time"

// ----------------------- core types (sub-block) ---------------------

// Amount of anything measurable.
type Amount interface {
	Amount() float64
}

// Tstamp returns the time that something happened.
type Tstamp interface {
	Tstamp() time.Time
}

// Location returns the general location of something.
type Location interface {
	Location() string // plain text
}

// Name returns a name, first and last, nickname, whatever.
type Name interface {
	Name() string
}

// Email returns a standard email address.
type Email interface {
	Email() string
}

// URL returns a standard URL way to locate something.
type URL interface {
	URL() string
}

// -------------------- block types (sub-document) --------------------

// Para returns a single paragraph of CommonMark markup text.
type Para interface {
	Para() string // CommonMark
}

// Entry is used to post timestamped notes with an optional amount.
type Entry interface {
	Tstamp
	Amount
	Para
}

// Event is something that happened or will happen at a place and time.
type Event interface {
	Tstamp
	Location
	Para
}
