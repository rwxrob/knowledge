package pandoc

import (
	"fmt"

	"gitlab.com/rwx.gg/kn/knowledge"
)

// Build renders all the Nodes concurrently.
func Build(b *knowledge.Base) error {
	if b == nil {
		return fmt.Errorf("Base is nil.")
	}
	b.MakeManifest()
	fmt.Println("World Build")
	return nil
}
