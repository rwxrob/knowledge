package pandoc

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/rwx.gg/kn/knowledge"
	"gitlab.com/rwx.gg/kn/knowledge/util"
)

func TestMain(m *testing.M) {
	os.Remove("../../testing/kbase1/something/index.html")
	os.Exit(m.Run())
}

func Test_Render(t *testing.T) {
	b, err := knowledge.NewBase("../../testing/kbase1")
	n, err := b.Node("something")
	require.Nil(t, err)
	Render(n)
	require.True(t, util.FileContains("../../testing/kbase1/something/index.html", "<p>Hey there."))
}
