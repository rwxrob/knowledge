package pandoc

import (
	"fmt"

	"gitlab.com/rwx.gg/kn/knowledge"
	"gitlab.com/rwx.gg/kn/knowledge/util"
)

// Render turns the README.md file within a Node into a web page using
// Pandoc's default HTML output. If the Node .kn/pandoc directory
// contains a template.html file it will be used instead of that defined
// as the default for the knowledge base
// (Base.Dir+".kn/pandoc/templates/main.html").  If styles.css is found
// in the knowledge.Node the xstyles template variable will be set to
// it. Calls to Run() are effectively asynchronous since nothing touches
// anything outside of a specific Base.Node.
//
func Render(n *knowledge.Node) error {

	if n == nil {
		return fmt.Errorf("Node is nil.")
	}

	// starting args
	args := []string{
		"pandoc", "-s", "--quiet",
		"--output=" + n.File("index.html"),
		"--data-dir=" + n.Base.File(".kn", "pandoc"),
	}

	// localized template check
	t := n.File("template.html")
	if n.HasFile("template.html") {
		args = append(args, "--template="+t)
	} else {
		args = append(args, "--template=main")
	}

	// localized styles.css check
	if n.HasFile("styles.css") {
		args = append(args, "--metadata=xstyles:styles.css")
	}

	// TODO detect the Type and match template

	args = append(args, n.READMEmd)
	return util.RunCommand(args...)
}
